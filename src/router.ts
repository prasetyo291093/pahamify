import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

import Index from "./views/index.vue";
import Airlines from "./views/airplane.vue";
import Simulation from "./views/simulation/index.vue"

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "index",
    component: Index,
  },
  {
    path: "/airlines",
    name: "airlines",
    component: Airlines,
  },
  {
    path: "/simulation",
    name: "simulation",
    component: Simulation,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

export default router;
