import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './assets/main.css';

import DashboardLayout from './components/Layout/DashboardLayout.vue';

const app = createApp(App);

app.component('default-layout', DashboardLayout);

app.use(router);
app.mount('#app');
