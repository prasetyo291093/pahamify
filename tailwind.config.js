const { width } = require('tailwindcss/defaultTheme');
const defaultTheme = require('tailwindcss/defaultTheme');

whitelist = ["gray", "red", "orange", "yellow", "green", "teal", "blue", "purple", "pink"].reduce(
  (result, color) => result.push(`text-${color}-600`, `bg-${color}-600`, `bg-${color}-500`) && result, [])

module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: ['./index.html', './src/**/*.{vue, js}'],
    options: {
      whitelist,
    }
  },
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      fill: {
        grey: '#A7A7A7',
        white: '#FFFFFF',
      },
      colors: {
        primary: '#2E5DB8',
        blueBro: '#008ECB',
        redBro: '#CC4C4C',
        greenBro: '#02AB1D',
        disabled: '#B3B3B3',
        error: '#FF7474',
        grey: '#A7A7A7',
        reasigned: '#C1C4C5'
      },
      height: {
        login: '839px',
        formLogin: '443px',
      },
      width: {
        login: '839px',
        formLogin: '403px',
      }
    },
  },
  variants: {
    extend: {
      fill: ['hover', 'focus'],
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
  ],
};
